require_relative "../env"

module Parsers
  class Pdf
    ORDER_REG = /\([0-9]{1,6}\/20[1,2]{1}[0-9]{1}\)/
    def initialize(record)
      @record = record
    end


    def people
      after_2016
    end

    def after_2016
      all_people.select{ |p| p[:year].to_i >= 2016 }
    end

    def all_people
      raw_people.map do |raw|
        name = raw.split("(")[0].sub(/[0-9]+\./, "").strip
        order_id = raw.scan(/\(([^\)]+)\)/).last.first

        {
          name: name,
          order_id: order_id,
          year:  order_id.split("/")[1].to_i,
        }
      end
    end

    def raw_people
      result = []
      the_pdf.pages.each do |p|
        result.push(*p.text.split("\n").select{ |x| x.match? ORDER_REG })
      end
      result
    end

    def the_pdf
      @the_pdf ||= PDF::Reader.new @record.file_path
    end
  end
end
