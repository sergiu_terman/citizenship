require 'yaml'
require 'recursive-open-struct'

class Config
  class << self
    def method_missing(method_sym, *arguments, &block)
      smart_hash = RecursiveOpenStruct.new load_config

      smart_hash.send method_sym
    end

    def load_config
      @config ||= YAML.load(File.read("./config.yaml"))
    end
  end
end
