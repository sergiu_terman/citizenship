require_relative 'env'

class Init
  # create database
  class << self
    def db_init
      Database.create_tables
    end

    def process_pdfs
      Runners::Pdf.new.start
    end

    def send_emails
      Runners::Notification.new.start
    end
  end
end
