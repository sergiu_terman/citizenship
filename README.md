### Setup

#### Clone the source code
```bash
git clone git@bitbucket.org:sergiu_terman/citizenship.git
cd citizenship
```
#### Configure mailgun
The repository is using mailgun to send email notifications.
* Create a [mailgun](https://www.mailgun.com/) account
* Go on **Domains** section and ensure you have a sandboxed domain
* Click on the sandbox domain and go to **Authorized Recipients**
* Add and verify the emails that you want to send the email tonifications
#### Create & edit config files
```bash
cp config.example.yaml config.yaml
```
* Add the emails to send the notifications (the one added in mailgun)
* Add the mailgun keys
* In **orders** section you have to include all the orders up until your own, i.e. your order is registered on 32542/2018. Then you would have to introduce:
```yaml
orders:
  in_2016: 114912
  in_2017: 94556
  current: 32542
```
These values are used to estimate the date when your order will be processed

The program starts to count from 2016, that's why it's importat to start with 2016. 

* In the **search** sectio introduce your name keyword and your order keyword. The values are used to notify if your order was already processed


### To play around with it
* Install Docker
* Create the database
```bash
docker run --rm -w "/root/citizenship" -v "$(pwd):/root/citizenship" sergiuterman/citizenship make db
```
* Run once to process, download and parse order pdfs, plus send the notifications at the end
```bash
docker run --rm -w "/root/citizenship" -v "$(pwd):/root/citizenship" sergiuterman/citizenship make start
```

### Start the app console
* Start a container
```bash
docker run --name bob -d -v "$(pwd):/root/citizenship" sergiuterman/citizenship sleep 300000
```

* Open a console
```bash
docker exec -ti -w "/root/citizenship" bob make console
```

* Run few commands
```Ruby
Pdf.count
Person.count
Person.where(year: 2017).count
Report.new.estimate_call
```
### Run from a cront
* Create an executable
```bash
mkdir ~/bin
touch ~/bin/run-parser
chmod +x ~/bin/run-parser
```
* Create the cronlog dir
```
mkdir -p ~/.cron
```

* Populate the executable with the following
```bash
#!/bin/sh
set -ex

home_dir=/path/to/your/home/dir
project_dir=/path/to/the/project/dir

/usr/local/bin/docker run --rm -w "/root/citizenship" \
  -v "$project_dir:/root/citizenship" \
  sergiuterman/citizenship make start >> $home_dir/.cron/exec.log 2>&1
```

* Add the cron in `crontab -e` that will run every 20 minutes
```
*/20 * * * * /your/home/dir/bin/run-parser
```
