require_relative "../env"

module Fetchers
  class Pdf
    DOTTED_DATE = /[0-9]{2}\.[0-9]{2}\.[0-9]{4}/
    LODASH_DATE = /[0-9]{2}_[0-9]{2}_[0-9]{4}/
    DOMAIN = "cetatenie.just.ro"

    def list
      after_2016
    end

    private

    def after_2016
      valid_urls.uniq{ |u| u[:name] }
    end

    def valid_urls
      result = []
      all_urls.each do |u|
        m = u.match(DOTTED_DATE)
        if !m.nil? && m.size > 0
          result.push(build_url(u, m[0]))
          next
        end

        m = u.match(LODASH_DATE)
        if !m.nil? && m.size > 0
          result.push(build_url(u, m[0].gsub("_", ".")))
          next
        end
        result.push(build_url(u, "01.01.2016"))
      end
      result
    end

    def build_url(u, date)
      {
        url: URI::join("http://#{DOMAIN}", u).to_s,
        date: Date.parse(date),
        name: u.split("images/")[1]
      }
    end

    def all_urls
      date_2016 = Date.new(2016, 1, 1)
      urls = []
      noko.css('.item-page li.first-child').each do |li|
        d = Date.parse li.css('strong')[0].text.strip.gsub("\u00A0", "")
        next if  d < date_2016
        urls += li.css('a').map{|x| x["href"]}.select{|u| u.include? ".pdf"}
      end
      urls
    end


    def noko
      Nokogiri::HTML(raw_page)
      # Nokogiri::XML(raw_page) # when reading from file
    end


    def raw_page
      begin
        retries ||= 0
        @raw_page ||= Net::HTTP.get(DOMAIN, "/index.php/ro/ordine/articol-11")
        # File.write("data/articol-11", @raw_page)
        # @raw_page
        # @raw_page ||= File.open("data/articol-11").read
      rescue
        retry if (retries += 1) < 3
      end
    end
  end
end
