require_relative "../env"

class Notification
  attr_reader :client

  def initialize
    @client = Mailgun::Client.new Config.mailgun.key
  end

  def send!
    send_report
    send_found
  end

  def send_report
    receivers.each do |receiver|
      e = build_report_email(receiver)
      client.send_message domain, e
    end
  end

  def build_report_email(receiver)
    {
      from: Config.mailgun.sender,
      to: receiver,
      subject: "[Articol 11] Report",
      html: report_email_body
    }
  end

  def report_email_body
    r = Report.new
    @report_email_body ||= %Q(Totally processed: #{r.processed_people}<br />
                              Remaining to process: #{r.until_me} <br />
                              Estimated daily throughput: #{r.daily_process_throughput}<br />
                              Estimated monthly throughput: #{r.daily_process_throughput * 30}<br />
                              Estimated call: #{r.estimate_call}<br />
                              Max for 2017: #{r.max_2017}<br />)

  end

  def send_found
    person = find_person
    if person.nil?
      Log.info("Person yet not found")
      return
    end
    Log.info("PERSON FOUND!!!!!!")
    receivers.each do |receiver|
      e = build_found_email(receiver, person)
      client.send_message domain, e
    end
  end

  def find_person
    from_order = Person.where(order_id: Config.search.order)
    return from_order.first unless from_order.empty?
    from_name = Person.where(Sequel.like(:full_name, "%#{Config.search.name}%"))
    return from_name.first unless from_name.empty?
    nil
  end


  def build_found_email(receiver, person)
    {
      from: Config.mailgun.sender,
      to: receiver,
      subject: "[Articol 11] You are the chosen one",
      html: found_email_body(person)
    }
  end

  def found_email_body(person)
    @found_email_body ||= %Q(You've been accepted <a href="http://cetatenie.just.ro/index.php/ro/ordine/articol-11">order</a><br />
    The order is <a href="#{person.pdf.url}">here</a>)
  end

  def domain
    Config.mailgun.domain
  end

  def receivers
    Config.emails
  end
end
