require 'sqlite3'
require 'sequel'
require 'json'
require 'pry'
require 'nokogiri'
require 'numbers_in_words'
require 'cgi'
require 'date'
require 'uri'
require 'mailgun'
require 'net/http'
require 'open-uri'
require 'pdf-reader'


require_relative "./fetchers/pdf"
require_relative "./runners/pdf"
require_relative "./parsers/pdf"
require_relative "./report/report"
require_relative "./notifier/notify"

require_relative './conf'
require_relative './log'
require_relative './database'
