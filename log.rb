require "logger"


class Log
  class << self
    def info(message)
      logger.info(message)
    end

    private

    def logger
      @logger ||= Logger.new("data/info.log")
    end
  end
end
