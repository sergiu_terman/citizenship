require_relative 'env'

DB = Sequel.connect("sqlite://data/the.db")

class Database
  class << self
    def create_tables
      DB.create_table :pdfs do
        primary_key :id

        Date        :created_at
        DateTime    :updated_at

        String      :url,         unique: true
        String      :name
        Date        :issued_at

        FalseClass  :parsed,      default: false
      end

      DB.create_table :people do
        primary_key :id
        foreign_key :pdf_id, :pdfs

        DateTime    :created_at
        DateTime    :updated_at

        String      :full_name
        String      :order_id
        String      :year
      end

      DB.create_table :statuses do
        primary_key :id

        Date        :created_at
        DateTime    :updated_at

        DateTime    :checked_at
        DateTime    :notified_at
      end
    end
  end
end


begin
  class Pdf < Sequel::Model
    plugin :timestamps
    one_to_many :people

    def download
      return if downloaded?
      open(file_path, "wb") do |f|
        f << open(url).read
      end
      Log.info("Dowloaded pdf: #{name}")
    end

    def downloaded?
      File.file?(file_path)
    end

    def file_path
      safe_name = name.gsub("/", "_")
      "data/downloads/#{safe_name}"
    end
  end

  class Person < Sequel::Model
    plugin :timestamps
    many_to_one :pdf
  end

  class Status < Sequel::Model
    plugin :timestamps

    def self.todays
      q = Status.where(created_at: (Date.today - 1)..Date.today)
      if q.empty?
        Status.create()
      else
        q.first
      end
    end

    def checked?
      !checked_at.nil?
    end

    def checked!
      set(checked_at: DateTime.now)
      save
    end

    def notified?
      !notified_at.nil?
    end
  end
rescue => e
  p e
  p "NO DB!"
end
