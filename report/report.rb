require_relative "../env"

class Report
  def until_me
    total_queue - processed_people
  end

  def estimate_call
    days = (until_me.to_f / daily_process_throughput).to_i
    d = latest_pdf_created_at + days
    "#{d.day}.#{d.month}.#{d.year}"
  end

  def max_2017
    Person.where(year: 2017).map(&:order_id).map{ |id| id.split("/")[0].to_i}.max
  end

  def processed_people
    @processed_people ||= Person.count
  end

  def total_queue
    Config.orders.to_hash.values.sum
  end

  def daily_process_throughput(months: 15)
    start_from = Date.today
    months.times { start_from = start_from.prev_month }
    processed = Pdf.where(created_at: start_from..latest_pdf_created_at).map{ |p| p.people.count }.sum
    processed.to_f / (latest_pdf_created_at - start_from).to_i
  end

  def latest_pdf_created_at
    Pdf.where{created_at < Date.today}.last.created_at
  end
end
