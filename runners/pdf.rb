require_relative "../env"

module Runners
  class Pdf
    def initialize
      @current_people = Person.count
    end

    def start
      if checked_today?
        Log.info("Already checked today, skipping")
        return
      end
      the_actual_start
      after_start
    end

    def checked_today?
      Status.todays.checked?
    end

    def after_start
      Status.todays.checked!
    end

    def the_actual_start
      add_missing_pdfs
      download_pdfs
      parse_pdfs
      send_notification
    end

    def send_notification
      unless should_notify?
        Log.info("No new people found, will not send notifications")
        return
      end
      Notification.new.send!
    end

    def should_notify?
      Person.count != @current_people
    end

    def add_missing_pdfs
      pdf_name_set = Set.new ::Pdf.all.map(&:name)
      web_pdfs = Fetchers::Pdf.new.list.select { |p| !pdf_name_set.include? p[:name] }

      web_pdfs.each do |p|
        ::Pdf.create(url: p[:url], name: p[:name], created_at: p[:date])
      end
      Log.info("#{web_pdfs.count} new pdfs were found")
    end

    def download_pdfs
      ::Pdf.all.each{|p| p.download}
    end

    def parse_pdfs
      unparsed_pdfs = ::Pdf.where(parsed: false)
      people_count = 0
      unparsed_pdfs.to_a.each do |pdf|
        Parsers::Pdf.new(pdf).people.each do |person|
          ::Person.create(pdf_id: pdf.id, order_id: person[:order_id], full_name: person[:name], year: person[:year])
          people_count += 1
        end
        pdf.set(parsed: true)
        pdf.save
      end
      Log.info("Parsed #{unparsed_pdfs.count} pdfs and found #{people_count} new people")
    end
  end
end
