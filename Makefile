console:
	ruby -e "require('./env.rb');binding.pry"

start:
	ruby -e "require('./init.rb'); Init.process_pdfs"

db: remove_db
	ruby -e "require('./init.rb'); Init.db_init"

remove_db:
	rm -f ./data/the.db &> /dev/null
